#include <Wire.h>
#include <Adafruit_MotorShield.h>
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

bool motor(int M, int power) {
  Adafruit_DCMotor *Mx = AFMS.getMotor(M);
  Mx->setSpeed(abs(power));
  
  if (power > 0) {
    Mx->run(FORWARD);
    return true;
  }
  else if (power > 255) {
    return false;
  }
  if (power < 0) {
    Mx->run(BACKWARD);
    return true;
  }
  else if (power < - 255) return false;
  else if (power == 0) {
    Mx->run(RELEASE);
    delay(50);
    Mx->run(BRAKE);
  }
  else return false;
}

void drive(int power) {
  motor(1,power);
  motor(2,-1 * power);
}

void rotate(int power) {
  motor(1,power);
  motor(2,power);
}


//setting up ultrasound
#define trigPin 13
#define echoPin 12



void setup() {
  AFMS.begin();
Serial.begin (9600);
pinMode(trigPin, OUTPUT);
pinMode(echoPin, INPUT);

}


int distance() {
long duration, distance;
digitalWrite(trigPin, LOW); // Added this line
delayMicroseconds(2); // Added this line
digitalWrite(trigPin, HIGH);
// delayMicroseconds(1000); - Removed this line
delayMicroseconds(10); // Added this line
digitalWrite(trigPin, LOW);
duration = pulseIn(echoPin, HIGH);
distance = (duration/2) / 29.1;

if (distance >= 400 || distance <= 0){
return distance; //for now return distance no matter what
}
else return distance;

}

float increment=0.25; //used to increase the distance from the wall at which the robot stops each time.

void loop() {

//Serial.println(distance());
//delay(400);



if(distance() > 30+floor(increment)*30){

  drive(150);
  
}

else {drive(0);

  int I = 0; // introduce new variable for while loop

  for (int a = 0; a < 10; a = a + 1 ){
    if (distance() > 30 + floor(increment)*40){ // DecaChecking if it’s true
      I = I+1;
      delay(100);
      Serial.println(distance());
      Serial.println(millis());
    }
     else {
      delay(100);
      Serial.println(distance());
      Serial.println(millis());

     }

  }
  if (I > 0){
    
    drive(150);
  }
  else {

  rotate(-70); // depends on testing
  delay(4800);
  increment = increment + 0.25;
  
  }
}
}
