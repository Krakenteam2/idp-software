//
//  main.cpp
//  IDP coding testing
//
//  Created by Adrian del Ser on 07/11/2018.
//  Copyright © 2018 Adrian del Ser. All rights reserved.
//

#include <iostream>
#include<string>
using namespace std;

int k = 2 ; // testing to see if I can initialize a variable outside of a function and then call it within that function
int motor; // random variable that is called later for testing only


int sum(void){
    /*this is some random test function*/
    
    return 2+k;
}



void advance(void/*possibly include power variable here*/){
    //    writing a test function to simply run both motors forward simultaneously when called
    
    // Motor(A, power);
    // Motor(B, power);
    
}

void retreat(void/*possibly include power variable here*/){
    //    writing a test function to move backwards when called, amount should be specified after the function is called.
    
    // Motor(A, -power);
    // Motor(B, -power);
    // delay = ?
}

void stop(void){
    //    writing a function to stop both motors when called
    
    // Motor(A, power = 0);
    // Motor(B, power = 0);
    
}


void turnright(void/*include angle? */){
    //    writing a test function to turn right around itself when called
    
    
    // Motor(A, power);
    // Motor(B, -power);
    // delay = ?
}

void turnleft(void/*include direction and angle? */){
    //    writing a test function to turn left around itself when called
    
    
    // Motor(A, -power);
    // Motor(B, power);
    // delay = ?
}

void squarepath(void){
    //robot follows some path
    /*
     advance();
     delay = ?;
     stop();
     turnright();
     stop();
     advance();
     delay = ?;
     stop;
     turnright();
     stop();
     advance();
     delay = ?;
     stop;
     turnright();
     stop();
     advance();
     delay = ?;
     stop;
     turnright();
     
     \\test turning left as well?
     
     
     turnleft();
     
     
     */
}

//void conditional_advance (void/*insert condition that needs to be met to continue advance, for example sensor minimum distance from wall or something*/){
    
//    while(/*condition that needs to be met*/){
        // advance()
    
 //   }
    //stop()
//}
//*/




int main(void) {
    // insert code here...
    

    
    motor = 3;
    printf("%d",motor);
    
    printf("Hello, %d is the half of %d!\n", 2, 4);
    
    printf("%d",sum());
    
    //cout << "Hello, World!\n";
    //return 0;
    
}








