//Define Variables we'll be connecting to
#include <PID_v1.h>
double Setpoint, Input, Output;

void side_distance_correctpid(void);
int side_distance(void);
int target_distance;

//Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint,2,5,1, DIRECT);

void setup()
{
  //initialize the variables we're linked to
  Input = side_distance();
  Setpoint = target_distance;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
}

void loop()
{
  side_distance_correctpid();
}
