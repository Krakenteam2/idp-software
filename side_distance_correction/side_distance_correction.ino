#include <Wire.h>
#include <Adafruit_MotorShield.h>
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

bool motor(int M, int power) {
  Adafruit_DCMotor *Mx = AFMS.getMotor(M);
  Mx->setSpeed(abs(power));
  
  if (power > 0) {
    Mx->run(FORWARD);
    return true;
  }
  else if (power > 255) {
    return false;
  }
  if (power < 0) {
    Mx->run(BACKWARD);
    return true;
  }
  else if (power < - 255) return false;
  else if (power == 0) {
    Mx->run(RELEASE);
    delay(50);
    Mx->run(BRAKE);
  }
  else return false;
}

void drive(int power) {
  motor(1,power);
  motor(2,-1 * power);
}

void rotate(int power) {
  motor(1,power);
  motor(2,power);
}


//setting up ultrasound
#define trigPin 13
#define echoPin 12



void setup() {
  AFMS.begin();
Serial.begin (9600);
pinMode(trigPin, OUTPUT);
pinMode(echoPin, INPUT);

}


int distance() {
long duration, distance;
digitalWrite(trigPin, LOW); // Added this line
delayMicroseconds(2); // Added this line
digitalWrite(trigPin, HIGH);
// delayMicroseconds(1000); - Removed this line
delayMicroseconds(10); // Added this line
digitalWrite(trigPin, LOW);
duration = pulseIn(echoPin, HIGH);
distance = (duration/2) / 29.1;

if (distance >= 400 || distance <= 0){
return distance; //for now return distance no matter what
}
else return distance;

}

float increment=0.25; //used to increase the distance from the wall at which the robot stops each time.
int target_distance = distance();
int correction_delay = 200; //to be experimented with
int increment1 = 0; //allows us to delay the start of the program below
bool correction_in_progress = false;
int error = 2; //error in cm that the program allows when keeping a constant distance from the side wall
unsigned long timer = millis()-2000; //returns the current time in ms since the program started
int driving_power = 75;
int time_before_check = 5000; //time before error correction can run again


void loop() {

if (increment1==0){
  delay(3000);
  target_distance = distance();
  drive(150);
  delay(2000);
  drive(driving_power);
}



increment1 = increment1 +1;




/*bool correction_in_progress = false;*/

if (distance() >= target_distance - error && distance() <= target_distance + error){

   correction_in_progress = false;
   /*drive(150);*/

}

else if (distance() < target_distance - error && millis()-timer > time_before_check){
  /*motor(1,0);
  motor(2,-150);
  */
  Serial.println(millis()-timer);
    Serial.println(timer);

  

  
  rotate(-100);
  delay(correction_delay); //find this experementally 

  drive(driving_power);
  timer = millis();
}

/*else if (distance() < target_distance -2 && correction_in_progress = true){
  delay(0); \\do nothing
}
*/

else if (distance() > target_distance + error && millis()-timer > time_before_check){
  /*motor(1,150);
  motor(2,-0);
  */
  Serial.println(millis()-timer);
      Serial.println(timer);

  
  rotate(+100);

  delay(correction_delay); //find this experementally 

  drive(driving_power);
  timer = millis();
}

else /*(correction_in_progress = true*/delay(0); //do nothing


}
