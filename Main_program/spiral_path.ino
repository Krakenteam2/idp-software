




#include <Wire.h>
#include <Adafruit_MotorShield.h>
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

//Define Variables we'll be connecting to
#include <PID_v1.h>



void return_to_base();

void drive(int power);

void rotate(int power);
void spiral_path();
void return_to_base();
int decacheck(int MinDistance);
void side_distance_correct();
int side_distance();
int distance();
bool motor(int M, int power);
int calibrate(int pin);
void initial_calibration(void);
char colour(int pin);
void mine_check(void);
void initial_calibration_test(void);
void side_distance_correctpid(void);

//setting up ultrasound
#define trigPin 12
#define echoPin 13

int side_distance() {
long duration, distance;
digitalWrite(trigPin, LOW); // Added this line
delayMicroseconds(2); // Added this line
digitalWrite(trigPin, HIGH);
// delayMicroseconds(1000); - Removed this line
delayMicroseconds(10); // Added this line
digitalWrite(trigPin, LOW);
duration = pulseIn(echoPin, HIGH);
distance = (duration/2) / 29.1;

if (distance >= 400 || distance <= 0){
return distance; //for now return distance no matter what
}
else return distance;

}



//setting up front ultrasound
#define trigPin1 10
#define echoPin1 11


int distance() {
long duration1, distance1;
digitalWrite(trigPin1, LOW); // Added this line
delayMicroseconds(2); // Added this line
digitalWrite(trigPin1, HIGH);
// delayMicroseconds(1000); - Removed this line
delayMicroseconds(10); // Added this line
digitalWrite(trigPin1, LOW);
duration1 = pulseIn(echoPin1, HIGH);
distance1 = (duration1/2) / 29.1;

if (distance1 >= 400 || distance1 <= 0){
return distance1; //for now return distance no matter what
}
else return distance1;

}











bool motor(int M, int power) {
  Adafruit_DCMotor *Mx = AFMS.getMotor(M);
  Mx->setSpeed(abs(power));
  
  if (power > 0) {
    Mx->run(FORWARD);
    return true;
  }
  else if (power > 255) {
    return false;
  }
  if (power < 0) {
    Mx->run(BACKWARD);
    return true;
  }
  else if (power < - 255) return false;
  else if (power == 0) {
    Mx->run(RELEASE);
    //delay(50); //this is if I want the car to stop slower
    Mx->run(BRAKE);
  }
  else return false;
}






///////////////////////////////////////// LDR CALIBRATION //////////////////////////////////////////

int calibrate(int pin) { // Calibrate pin 'pinnumber.' Note: Each pin must be calibrated separately. See void loop
  int times = 10; // Set number of readings to average over
  float sensorSum = 0;

  for(int i = 0; i < times; i++) { // Take the average of 'times' readings as the background
    sensorSum += analogRead(pin);
    delay(10);
    } 
    return sensorSum / times;
}

// Necessary code to calibrate all LDRs, only needs to run once:



void initial_calibration(void){
  //this is the command to put in the main loop.
  //float calibration[8];
  
  if (calibration_increment == 0) {

    rotate(turning_power);
    delay(turning_time / 2);
    drive(driving_power);
    delay(2000);
    drive(0);
    delay(500);
  for(int pin = min_colour_pin; pin < max_colour_pin; pin++) {    // This for loop NEEDS to be at the start of the main program
  calibration[pin-min_colour_pin] = calibrate(pin);
  calibration_increment += 1;
  //Serial.println(calibration_increment);
  //Serial.println(calibration[pin-min_colour_pin]);
  //Serial.println(calibrate(pin));
  }

    drive(- driving_power);
    delay(2000);
    rotate(- turning_power);
    delay(turning_time / 2);
    drive(driving_power);
    delay(1300);
    rotate(turning_power);
    delay(turning_time);
    drive(0);
  }
//Serial.println(calibration[0]);
}








void initial_calibration_test(void){
  //this is the command to put in the main loop.
  //float calibration[8];
  
  if (calibration_increment == 0) {

   /* rotate(turning_power);
    delay(turning_time / 2);
    drive(driving_power);
    delay(2000);
    drive(0);
    delay(500);*/
  for(int pin = min_colour_pin; pin < max_colour_pin; pin++) {    // This for loop NEEDS to be at the start of the main program
  calibration[pin-min_colour_pin] = calibrate(pin);
  calibration_increment += 1;
  //Serial.println(calibration_increment);
  //Serial.println(calibration[pin-min_colour_pin]);
  //Serial.println(calibrate(pin));
  }
/*
    drive(- driving_power);
    delay(2000);
    rotate(- turning_power);
    delay(turning_time / 2);
    drive(driving_power);
    delay(500);
    rotate(turning_power);
    delay(turning_time);
    drive(0);*/
  }
//Serial.println(calibration[0]);
}











///////////////////////////////////////// LDR COLOUR SENSING //////////////////////////////////////

char colour(int pin) {            // Standard usage: colour(pin) - returns the first letter of the detected colour - red, yellow or black

  if (pin == 8){
    Black = 69;
    RedLower = 70;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
    }

  else if (pin == 9){
    Black = 65;
    RedLower =66 ;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
  }

   else if (pin == 10){
    Black = 79;
   RedLower = 80;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
  }

   else if (pin == 11){
    Black = 75;
    RedLower = 76;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
  }

   else if (pin == 12){
    Black = 60;
    RedLower = 61;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
  }

   else if (pin == 13){
    Black = 60;
    RedLower = 61;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
  }

   else if (pin == 14){
    RedLower = 1.5;
    RedUpper = 4;
    YellowLower = 5;
    YellowUpper = 15;
  }

   else if (pin == 15){
   Black = 75;
    RedLower = 76;
    RedUpper = 150;
    YellowLower = 151;
    YellowUpper = 4000;
  }

  
  int times = 2; // Number of times to verify value for 
  //Serial.println(calibration[0]);
  float sensorValue = analogRead(pin) - calibration[pin-min_colour_pin];
  bool test;
  //Serial.println(sensorValue);
  //Serial.println(calibration[pin]);
  if (sensorValue <= Black) { // Testing for colour black
    test = true;
    for(int i = 0; i < times; i++) {
      if (sensorValue > Black) {
        test = false;
        delay(2);
        break;
      }
    }
    if (test == true) {
      return 'b';
    }
  }

  if (sensorValue >= RedLower && sensorValue < RedUpper) { // Testing for colour red
    test = true;
    for(int i = 0; i < times; i++) {
      if (sensorValue < RedLower || sensorValue > RedUpper) {
        test = false;
        delay(2);
        break;
      }
    }
    if (test == true) { 
      return 'r';
    }
  }

  if (sensorValue > YellowLower && sensorValue < YellowUpper) { // Testing for colour yellow
    test = true;
    for(int i = 0; i < times; i++) {
      if (sensorValue < YellowLower || sensorValue > YellowUpper) {
        test = false;
        delay(2);
        break;
      }
    }
    if (test == true) {
      return 'y';
    }
  }
  
}




////////////////////////////////////////


















/////////////////////////////////////////////////////////


void drive(int power) {
  motor(1,-1 * power);
  motor(2, 1.05 * power);
}

/////////////////////////////////////////



void rotate(int power) {
  motor(1,power);
  motor(2,power);
}















///////////////////////////////










void spiral_path() {

//Serial.println(distance());
//delay(400);



if(distance() > 40+floor(increment)*25){

  drive(150);
  
}

else {drive(0);

  int I = 0; // introduce new variable for while loop

  for (int a = 0; a < 10; a = a + 1 ){
    if (distance() > 40 + floor(increment)*25){ // DecaChecking if it’s true
      I = I+1;
      delay(100);
      /*Serial.println(distance());
      Serial.println(millis());*/
    }
     else {
      delay(100);
      /*Serial.println(distance());
      Serial.println(millis());*/

     }

  }
  if (I > 0){
    
    drive(150);
  }
  else {

  rotate(-turning_power); // depends on testing
  delay(turning_time);
  increment = increment + 0.25;
  target_distance = side_distance();
  
  }
}
}










//////////////////////////////////////////////////////















void return_to_base(){

  drive(0);



while(returned == 0){

  
  if (increment - (long)increment == 0.0){
  //facing west
        rotate(turning_power);
        delay(turning_time);
        drive(0);
        increment = 0.75;
  
  
  }//end of west statement

  else if (increment - (long)increment == 0.25){
    

  //facing North, need to turn to face south

        rotate(turning_power);
        delay(2*turning_time);
        drive(0);
        increment = 0.75;
        
  } //end of North statement

  else if (increment - (long)increment == 0.50){

    //facing East, need to turn to face south

        rotate(-turning_power);
        delay(turning_time);
        drive(0);
        increment = 0.75;
        

  } //end of East statement

  else if (increment - (long)increment == 0.75){

    //facing south, need to turn south

        
       

  } // end of south statement

  
    
    
    if(increment2 == 0){
      //increment2 must be reset to 0 after every turn
    if (turned_to_base == 0){
    
    target_distance = side_distance();

    }

    else {
    target_distance = 9; //ideal side distance
      
    }
    increment2 = increment2 + 1;
    }



    if (turned_to_base == 0){

      //do the stuff below until the final turn to base is complete
    
    if(distance() > 7){

      drive(driving_power);
      side_distance_correct(2000);
  
    }
    
    else {
      drive(0);
      //delay(500);
      int check = decacheck(7);
      //Serial.println(check);
      if (check == 1){
      //we are close to the actual wall
              rotate(-turning_power);
              delay(turning_time);
              drive(0);
              increment2 = 0; //resetting the increment so the target distance is updated 
              increment2 = 0.75;//since we are now facing west but must keep this as south otherwise it will turn again
      turned_to_base = 1;
       }
      else drive(driving_power);
      }
   }

   else {

    //we have now made the final turn to the west, facing the base;

    if(distance() > 20){

      drive(driving_power);
      side_distance_correct(2000);
  
    }
    
    else {
      drive(0);
      //delay(500);
      int check = decacheck(20);
      //Serial.println(check);
      if (check == 1){
      //we are close to the actual wall
              //rotate(turning_power);
              //delay(turning_time);
              drive(0);

              //rotate(-turning_power);
              //delay(turning_time);
      returned = 1;
      delay(100000);
       }
      else drive(driving_power);
      }
    
   }
   

} // end of while loop
} // end of main function





//////////////////////////////////////////////////

















int decacheck(int MinDistance) {

//this is the safety condition in case someone waves their hand over the front sensor

drive(0);

  int I = 0; // introduce new variable for while loop

  for (int a = 0; a < 10; a = a + 1 ){
    //Serial.println(Distance);
    if (distance() > MinDistance){ // DecaChecking if it’s true
      I = I + 1;
      delay(100);
      //Serial.println(distance());
      //Serial.println(I);
      //Serial.println(millis());
    }
     else {
      delay(100);
      //Serial.println(distance());
      
      //Serial.println(millis());

     }

  }
  if (I > 0){
    //Serial.println(0);
    //drive(driving_power);
    return 0; //false alarm
  }
  else {
   //Serial.println(1);

  //drive(0);
  return 1; //actually near a wall
  
  }
}  























////////////////////////////////////////////////////////////////

























void side_distance_correct (int time_before_check){
  //put this first, before spiral path, so the delay can happen
  if (increment1==0){
  delay(3000);
  target_distance = side_distance();
  /*drive(150);
  delay(2000);*/
  drive(driving_power);
  increment1 = increment1 +1;
}








/*bool correction_in_progress = false;*/

if (side_distance() == target_distance - error && side_distance() <= target_distance + error){

//   correction_in_progress = false;
   /*drive(150);*/

}

else if (side_distance() < target_distance - error && millis()-timer > time_before_check){
  /*motor(1,0);
  motor(2,-150);
  */
  //Serial.println(millis()-timer);
   // Serial.println(timer);

  

  
  rotate(-100); //this is the original one, it works

  //motor(1, - driving_power);
  //motor(2, driving_power - correction_power);
  delay(correction_delay); //find this experementally  //this works

  drive(driving_power); //this works
  timer = millis();
}

/*else if (distance() < target_distance -2 && correction_in_progress = true){
  delay(0); \\do nothing
}
*/

else if (side_distance() > target_distance + error && millis()-timer > time_before_check){
  /*motor(1,150);
  motor(2,-0);
  */
  //Serial.println(millis()-timer);
      //Serial.println(timer);

  
  rotate(+100); //this works

  //motor(1, - driving_power + correction_power);
  //motor(2, driving_power);

  delay(correction_delay); //find this experementally 

  drive(driving_power); //this works
  timer = millis();
}

else /*(correction_in_progress = true*/delay(0); //do nothing
}







////////////////////////////////////////////////////////////////////////////////////








void side_distance_correct_better (void){
  //put this first, before spiral path, so the delay can happen
  if (increment1==0){
  delay(3000);
  target_distance = side_distance();
  /*drive(150);
  delay(2000);*/
  drive(driving_power);
  increment1 = increment1 +1;
}








/*bool correction_in_progress = false;*/

if (side_distance() == target_distance){


   //drive(driving_power);

}

else if (side_distance() < target_distance){

  int diff = abs(side_distance() - target_distance);
  motor(1,-driving_power);
  motor(2, driving_power - (diff * correction_power) );
  
 

 
}



else if (side_distance() > target_distance){
  //opposite of above
 int diff = abs(side_distance() - target_distance);
  motor(2,driving_power);
  motor(1, -driving_power + (diff * correction_power) );
}

else /*(correction_in_progress = true*/delay(0); //do nothing
}









///////////////////////////////////////////////












String get_coordinates(void) {

  String xthing = "x = ";
  String ything = "y = ";

  int H = 235; // side wall dimension Y
  int D = 235; //side wall dimension X

if (increment - (long)increment == 0.0){

  int xcoord = distance();
  int ycoord = side_distance();

  String coord = xthing + xcoord + " " + ything + ycoord;
  
  return coord;
  
  }

else if (increment - (long)increment == 0.25){

  int xcoord = side_distance();
  int ycoord = H - distance();

  String coord = xthing + xcoord + " " + ything + ycoord;
  
  return coord;
  
  }

else if (increment - (long)increment == 0.50){

  int xcoord = D - distance();
  int ycoord = H - side_distance();

  String coord = xthing + xcoord + " " + ything + ycoord;
  
  return coord;
  
  }

else if (increment - (long)increment == 0.75){

  int xcoord = D - side_distance();
  int ycoord = distance();

  String coord = xthing + xcoord + " " + ything + ycoord;
  
  return coord;
  
  }

else {
  String what = "what is going on, increment is not valid";

  return what;
}
  
}











////////////////////////////////////////////////////////////










void mine_check(void){

int yellow_sensors[] = {50, 50, 50, 50, 50, 50, 50, 50}; //this will be array of sensors detecting a yellow, currently all 50 to make sure they are bigger than any sensor pin numbers.

int red_sensors[] = {50, 50, 50, 50, 50, 50, 50, 50}; //this will be array of sensors detecting a red, currently all 50 to make sure they are bigger than any sensor pin numbers.

int yellow_increment = 0; // increment to increase every time a sensor is reading yellow
int red_increment = 0; // increment to increase every time a sensor is reading red


for (int c = min_colour_pin; c < max_colour_pin; c = c + 1 ){
  
  
  if (colour(c) == 'y'){
    //drive(0);
   yellow_sensors[c - min_colour_pin] = c; //add the number of the pin that detected the yellow to the yellow array
   yellow_increment = yellow_increment + 1; //increment the yellow
  }
  else if (colour(0) == 'r'){
    //drive(0);
    red_sensors[c - min_colour_pin] = c; //add the number of the pin that detected the red to the red array
    red_increment = red_increment + 1; //increment the red
  }

  else {}

} //end of for loop to run through all sensors to check for mines

if (yellow_increment > 1 or red_increment > 1){
  drive(0); //stop


    if (yellow_increment > 1 and red_increment == 0){
    delay(5000);
      //raise plough
      //drive forward by a certain amount
      //lower plough
      //drive backwards to return to close to original position
      //print coordinates
      //carry on driving back to normal
      Serial.println("Yellow mine detected");
    }
    
    else if (yellow_increment == 0 and red_increment > 0){
          //print coordinates
          //avoid red mine somehow
      delay(5000);
      Serial.println("Red mine detected");
    }
    
    else if (yellow_increment > 0 and red_increment > 0){
      delay(5000);
      //two different coloured mines detected
      //print coordinates
      //lets collect them both? (so raise plough and run over, and lower plough)
      Serial.println("Red and yellow mines detected");
    }
  
}// end of if statement that does stuff if either red or yellow increments are non zero

else {}

}

 

void minecheck_test (void){

drive(driving_power);
if (increment - (long)increment == 0.0 and distance() < 70){
  drive(driving_power);
}
else if (colour(8) != 'b' or colour(9) != 'b' or colour(10) != 'b'/* or colour(11) != 'b' or colour(12) != 'b' or colour(13) != 'b' /*or colour(14) != 'b' */or colour(15) != 'b'){
    drive(0);
    drive(50);
    delay(mine_delay);
    drive(0);

    if (colour(8) == 'y' or colour(9) == 'y' or colour(10) == 'y' /*or colour(11) == 'y'/or colour(12) == 'y' or colour(13) == 'y' /*or colour(14) != 'b'*/ or colour(15) == 'y'){

    digitalWrite(7, HIGH);
    String yellowcoord = "Yellow mine detected at: " + get_coordinates();
    Serial.println(yellowcoord);
    delay(3000);
    //drive(-driving_power);
    motor(3,200);
    drive(driving_power);
    delay(eating_delay);
    drive(0);
    motor(3,0);
    drive(-driving_power);
    delay(1.1* eating_delay);
   digitalWrite(7, LOW);
   
    drive(driving_power);
  }
  else {
    digitalWrite(6, HIGH);
    String redcoord = "Red mine detected at: " + get_coordinates();
    Serial.println(redcoord);
     drive(driving_power);
    delay(3000); // stop for 3 seconds


    // CODE FOR AVOIDING RED MINES
    if(increment > 1){
     drive(-driving_power); //move back
    delay(1500);
    rotate(turning_power);//turn left to face left
    delay(turning_time);
    drive(driving_power);//forward (facing left)
    delay(3500);
    rotate(-turning_power); //turn right to face forward
    delay(turning_time);
    drive(driving_power); //move forward past the mine
    delay(6200);
    rotate(-turning_power); //turn right to face right
    delay(turning_time);
    drive(driving_power);//move forward (facing right)
    delay(3500);
    rotate(turning_power);//turn left to face forward
    delay(turning_time);
    drive(0); //stop
    
    }
    //END OF CODE TO AVOID RED MINES
    digitalWrite(6,LOW);
    
    drive(driving_power);
  }

}
}




void conditional_return(void){
  if(millis() > 420000){
    return_to_base();
  }
}



/*void side_distance_correctpid(void){
  Input = side_distance();
  myPID.Compute();
  Serial.println(Output);
}
*/



















void setup() {
  AFMS.begin();
Serial.begin (9600);
pinMode(trigPin, OUTPUT);
pinMode(echoPin, INPUT);
pinMode(trigPin1, OUTPUT);
pinMode(echoPin1, INPUT);
pinMode(8, OUTPUT);
pinMode(9, OUTPUT);
// initialize digital pin LED_BUILTIN as an output.
  pinMode(6, OUTPUT);
pinMode(7, OUTPUT);
}
