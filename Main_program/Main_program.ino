void spiral_path();
void return_to_base();
void drive(int power);
void side_distance_correct(int time_before_check);
int distance();
int decacheck(/*int Distance, */int MinDistance); //returns 1 if true and 0 if false
int side_distance();
String get_coordinates(void);
void initial_calibration(void);
int calibrate(int pin);
char colour(int pin);
void mine_check(void);
void minecheck_test (void);
bool motor(int M, int power);
void rotate(int power);
void side_distance_correct_better (void);
void initial_calibration_test(void);

int Setpoint, Input, Output;

void side_distance_correctpid(void);
//Specify the links and initial tuning parameters
//PID myPID(&Input, &Output, &Setpoint,2,5,1, DIRECT);



int target_distance = side_distance();
int correction_delay = 150; //to be experimented with
int increment1 = 0; //allows us to delay the start of the program below
//bool correction_in_progress = false;
int error = 2; //error in cm that the program allows when keeping a constant distance from the side wall
unsigned long timer = millis()-2000; //returns the current time in ms since the program started
int driving_power = 100;
int turning_power = 60;
int turning_time = 4400; // how long to turn at -70 power to turn 90 degrees in milliseconds
int mine_delay = 300; // how long before the robot stops
int eating_delay = 3000;
int correction_power = floor(driving_power / 4);
//int time_before_check = 5000; //time before error correction can run again
float increment=0.25; //used to increase the distance from the wall at which the robot stops each time.
int increment2 = 0; //increment to set the target side distance to the current side distance only once
int returned = 0; //if back at base then 1, if not then 0.
int turned_to_base = 0; //this is true only once the final turn to base is complete

int min_colour_pin = 8; //smallest pin being used by the LDRs
int max_colour_pin = 16;//largest pin being used by the LDRs plus 1 (So if largest is 15, this should be 16)
int spiral_reduction = 30; //this is the amount by which we decrease the size of the spiral each time


// Important variables for colour detection:
float RedUpper = 4;
float YellowUpper = 15;
float YellowLower = 5;
float RedLower = 1.5;
float Black = 0;
int threshold = 5;
float calibration[8];
int calibration_increment = 0; //this is updated to 1 after colour sensors are calibrated
int rednumber = 0; //number of reds found
int yellownumber = 0; //number of yellows found



void loop() {
//digitalWrite(6, HIGH);





  
  //delay(100000);
initial_calibration();
side_distance_correct(3000);
spiral_path();
  //side_distance_correct_better();
//drive(driving_power);
//rotate(0);
//motor(2,0);

//delay(3000);

//motor(3,0);
//delay(3000);
//Serial.println(calibrate(8));
//colour(8);
//Serial.println(colour(9));
//Serial.println(colour(13));
//mine_check();
minecheck_test();
//delay(3000);
  // put your main code here, to run repeatedly:

//side_distance_correct(4000);

//Serial.println(distance());

//Serial.println(side_distance());

/*Serial.println(colour(15));
Serial.println(colour(14));
Serial.println(colour(13));
Serial.println(colour(12));
Serial.println(colour(11));
Serial.println(colour(10));
Serial.println(colour(9));
Serial.println(colour(8));*/
/*if(returned == 0) {
side_distance_correct(2000);
spiral_path();


if (increment == 0.75){
  return_to_base();
  
}
}
*/

//delay(500);



if(increment==3.5){
  return_to_base();
}


}
